# VMware_update

#### 介绍
&emsp;&emsp;每当Linux系统更新或者头文件更新后，VMware Workstation在运行时都会报错“vmware kernel module updater ERROR”。本脚本主要是Linux升级后解决VMware无法启动，通过运行脚本一键自动安装所需更新。


#### 使用说明

1.  下载"VMware-update.sh"
2.  授权chmod +x VMware-update.sh(可选)
3.  执行"./ VMware-update.sh"（无授权情况下执行“bash VMware-update.sh”）
4.  详细步骤参见[为极客而生](https://www.cnblogs.com/4geek/p/12187463.html)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
