# VMware_update

#### Description
Whenever the Linux system has updated or the header file has updated, VMware Workstation will report "VMware kernel module updater error" at runtime. This script is mainly used to solve the problem that VMware cannot start after upgrading Linux, and automatically install the required updates by running the script with one click.

#### Software Architecture
Software architecture description


#### Instructions

1. download"VMware-update.sh"
2. Authorization“chmod +x VMware-update.sh”(It's not necessary)
3. execute"./ VMware-update.sh"（if no authoriz execute“bash VMware-update.sh”）
**Detial steps please refer to:[F0rGeEk](https://www.cnblogs.com/4geek/p/12187463.html)**

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
